package main

import (
	"log"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/stripe/stripe-go/v74"
	"github.com/stripe/stripe-go/v74/subscription"
)

type SubscriptionInfo struct {
	Note       string `form:"note"`
	Importance string `form:"importance"`
}

func main() {
	api_key, found := os.LookupEnv("STRIPE_API_KEY")
	if !found || api_key == "" {
		log.Fatal("No Stripe API key was found; please provide it via the environment variable 'STRIPE_API_KEY'")
	}
	stripe.Key = api_key

	router := gin.Default()
	router.Static("/static", "./static")
	router.LoadHTMLGlob("templates/*")

	// TODO: move the logic for our endpoints to a separate file
	router.GET("/", func(c *gin.Context) {
		params := &stripe.SubscriptionListParams{}
		params.AddExpand("data.customer")
		i := subscription.List(params)
		var unhealthy_subscriptions, healthy_subscriptions []*stripe.Subscription
		for i.Next() {
			if i.Subscription().Status == "active" {
				healthy_subscriptions = append(healthy_subscriptions, i.Subscription())
			} else {
				unhealthy_subscriptions = append(unhealthy_subscriptions, i.Subscription())
			}
		}
		c.HTML(http.StatusOK, "subscriptions.tmpl", gin.H{
			"healthy_subscriptions":   healthy_subscriptions,
			"unhealthy_subscriptions": unhealthy_subscriptions,
		})
	})

	router.PUT("/subscriptions/:id", func(c *gin.Context) {
		id := c.Param("id")
		// Note: once we expand beyond a single field, probably better
		// to create a CustomerInfo struct and use c.Bind()
		var subscriptionInfo SubscriptionInfo
		c.Bind(&subscriptionInfo)
		params := &stripe.SubscriptionParams{}
		params.AddMetadata("note", subscriptionInfo.Note)
		params.AddMetadata("importance", subscriptionInfo.Importance)
		params.AddExpand("customer")
		s, err := subscription.Update(id, params)
		if err != nil {
			// TODO: returning a 500 here is a convenient lie--could be
			// more informative by examining the stripe error in more
			// detail (https://stripe.com/docs/api/errors/handling)
			c.AbortWithError(http.StatusInternalServerError, err)
		}
		c.HTML(http.StatusOK, "subscription_row.tmpl", s)
	})

	// Returns an "editable" version of the given subscription.
	// This is a concession to the htmx model; see https://htmx.org/examples/edit-row/
	router.GET("/subscriptions/:id/edit", func(c *gin.Context) {
		// This approach is simple, but inefficient--currently, this is
		// only called by the frontend, which _already_ has the actual
		// subscription data, it just needs it in <input> elements instead
		// of as text. The Stripe API call here isn't necessary, but it's
		// the path of least resistance, and keeps this endpoint general
		// for future use. Other possible implementations:
		//
		// 1. Do it all with Javascript in the frontend: probably the simplest
		// answer _for now_, but might lead to spaghetti code down the line if
		// we're just attaching random JS to every button... plus it's not the
		// htmx "way."
		//
		// 2. Have a _single_ endpoint to handle this: the client sends the
		// current data, and the server just responds with <inputs> wrapping
		// that data. Saves the Stripe API call; introduces a bit of
		// complexity.
		id := c.Param("id")
		params := &stripe.SubscriptionParams{}
		params.AddExpand("customer")
		s, err := subscription.Get(id, params)
		if err != nil {
			c.AbortWithError(http.StatusInternalServerError, err)
		}
		// Idea: if we also wanted to support JSON responses, could we
		// add a middleware layer that checks for the HX-Request header
		// (which is always set in our frontend requests), and only
		// render the template if that header is present?
		c.HTML(http.StatusOK, "subscription_row_edit.tmpl", s)
	})

	router.Run(":8080") // TODO: make configurable
}
